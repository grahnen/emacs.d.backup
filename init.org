#+PROPERTY: header-args :tangle yes
* Core Packages
** Directory
#+BEGIN_SRC emacs-lisp
  (setq load-prefer-newer t
	package-user-dir "~/.emacs.d/elpa")
#+END_SRC
** straight.el
#+BEGIN_SRC emacs-lisp
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
#+END_SRC
** use-package
#+BEGIN_SRC emacs-lisp
(straight-use-package 'use-package)
(setq straight-use-package-by-default t)
#+END_SRC
** org-mode
This is done early due to straight.el shennanigans.
#+BEGIN_SRC emacs-lisp
(require 'subr-x)
(straight-use-package 'git)

(defun org-git-version ()
  "The Git version of org-mode.
Inserted by installing org-mode or when a release is made."
  (require 'git)
  (let ((git-repo (expand-file-name
                   "straight/repos/org/" user-emacs-directory)))
    (string-trim
     (git-run "describe"
              "--match=release\*"
              "--abbrev=6"
              "HEAD"))))

(defun org-release ()
  "The release version of org-mode.
Inserted by installing org-mode or when a release is made."
  (require 'git)
  (let ((git-repo (expand-file-name
                   "straight/repos/org/" user-emacs-directory)))
    (string-trim
     (string-remove-prefix
      "release_"
      (git-run "describe"
               "--match=release\*"
               "--abbrev=0"
               "HEAD")))))

(provide 'org-version)

(straight-use-package 'org) ; or org-plus-contrib if desired

#+END_SRC
** Updates
#+BEGIN_SRC emacs-lisp
  (use-package auto-package-update
	       :config
	       (setq auto-package-update-delete-old-versions t
		     auto-package-update-hide-results t))

  (defun update-packages ()
    (interactive)
    (quelpa-self-upgrade)
    (auto-package-update-now))
#+END_SRC

** Keybindings
#+BEGIN_SRC emacs-lisp
  (use-package general
    :straight (:type git :host github :repo "noctuid/general.el"))
#+END_SRC
** Async
 #+BEGIN_SRC emacs-lisp
 (use-package async)
 #+END_SRC
** Delight
Removes things from the modeline
#+BEGIN_SRC emacs-lisp
  (use-package delight)
#+END_SRC
** Icons
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package all-the-icons)
  (use-package all-the-icons-dired
    :config
    (add-hook 'dired-mode-hook 'all-the-icons-dired-mode))
#+END_SRC
* Custom Functions
#+BEGIN_SRC emacs-lisp
  (defun reinstall-packages ()
    "Reinstall all emacs packages, by removing all directories"
    (interactive)
    (mapcar (lambda (dir) (delete-directory (concat user-emacs-directory dir) t)) '("straight" "elpa" "melpa")))

  (defun make-directory-if-not-exists (dir)
    "Create directory if it's not there"
    (interactive "D")
    (if (not (file-directory-p dir))
        (make-directory dir)))


  (defun eval-and-replace ()
    (interactive)
    (backward-kill-sexp)
    (condition-case nil
        (prin1 (eval (read (current-kill 0)))
               (current-buffer))
      (error (message "Invalid Expression")
             (insert (current-kill 0)))))


  (defmacro find-lambda (file &optional wk)
    (if (eq wk nil)
        `(lambda () (interactive) (find-file ,file))
      ``((lambda () (interactive) (find-file ,,file)) :wk ,(file-name-nondirectory ,file))))

  (defun sudo-edit (&optional arg)
    (interactive "P")
    (if (or arg (not buffer-file-name))
        (find-file (concat "/sudo:root@localhost:"
                           (ido-read-file-name "Find file(as root): ")))
      (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

  (defun okular-make-url ()
    (concat
     "file://"
     (expand-file-name (funcall file (TeX-output-extension) t)
                       (file-name-directory (TeX-master-file)))
     "#src:"
     (TeX-current-line)
     (TeX-current-file-name-master-relative)))

#+END_SRC
* Sane Defaults
** better-defaults.e
#+BEGIN_SRC emacs-lisp :tangle yes
(use-package better-defaults)
#+END_SRC
** Byte Compile Warnings
#+BEGIN_SRC emacs-lisp
(setq byte-compile-warnings '(not free-vars unresolved noruntime lexical make-local))
#+END_SRC
** Encoding (UTF-8)
#+BEGIN_SRC emacs-lisp
(set-selection-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-language-environment 'utf-8)
(setq locale-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
#+END_SRC
** Behaviour
#+BEGIN_SRC emacs-lisp
  (define-key key-translation-map (kbd "ESC") (kbd "C-g"))
  (setq sentence-end-double-space nil
        save-place-mode t
        global-auto-revert-mode 1
        global-auto-revert-non-file-buffers t
        global-auto-revert-buffers t
        auto-revert-verbose nil
        echo-keystrokes 0.1
        initial-scratch-message nil
        delete-selection-mode t 
        column-number-mode t
        inhibit-startup-message t)
#+END_SRC
** Line Wrapping
#+BEGIN_SRC emacs-lisp :tangle yes
(add-hook 'text-mode-hook (lambda () (visual-line-mode t)))
#+END_SRC
* Auto Tangling
#+BEGIN_SRC emacs-lisp
  (setq enable-local-eval t)

  (defun async-tangle-init ()
    (async-start
     `(lambda ()
       (require 'org)
       
       (org-babel-tangle-file
        (expand-file-name (concat user-emacs-directory "init.org")))

       (byte-compile-file
        (expand-file-name (concat user-emacs-directory "init.el")))
       (message "Compiled Init"))
     'ignore))
#+END_SRC

# Local Variables:
# eval: (add-hook 'after-save-hook 'async-tangle-init)
# End:

* Customize
#+BEGIN_SRC emacs-lisp
(setq custom-file (concat user-emacs-directory "/customize.el"))
(load-file custom-file)
#+END_SRC
* Completion
** FlyCheck
Currently disabled. Messes up ~after-save-hook~..

#+BEGIN_SRC emacs-lisp :tangle no
  (use-package flycheck
    :init (global-flycheck-mode))
#+END_SRC
** Company (In-buffer)
#+BEGIN_SRC emacs-lisp
  (use-package company
    :delight
    :custom
    (company-idle-delay 0.1)
    (company-minimum-prefix-length 1)
    (company-show-numbers nil)
    (company-tooltip-align-annotations t)
    (company-require-match nil)
    (global-company-mode t))

  (use-package company-box :after company
    :hook (company-mode . company-box-mode))

  (use-package company-quickhelp
    :hook (company-mode . company-quickhelp-mode))
#+END_SRC
** Ivy/Counsel (Minibuffers)
#+BEGIN_SRC emacs-lisp
  (use-package counsel)
  (use-package ivy
    :demand t
    :delight
    :config
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t
          enable-recursive-minibuffers t)
    :general
    ("C-s" 'swiper)
    ("M-x" 'counsel-M-x)
    ("C-x C-f" 'counsel-find-file))
#+END_SRC
* Backup and Autosaves
#+BEGIN_SRC emacs-lisp
  (defvar user-temporary-file-directory
    (concat temporary-file-directory user-login-name "/"))
  (make-directory user-temporary-file-directory t)
  (setq backup-by-copying t)
  (setq backup-directory-alist
	`(("." . ,user-temporary-file-directory)
	  (,tramp-file-name-regexp nil)))
  (setq auto-save-list-file-prefix
	(concat user-temporary-file-directory ".auto-saves-"))
  (setq auto-save-file-name-transforms
	`((".*" ,user-temporary-file-directory t)))
#+END_SRC

* Helper Packages
** Exec-path
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package exec-path-from-shell
    :config
    (exec-path-from-shell-initialize))
#+END_SRC
** Virtual Environment (python)
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package virtualenvwrapper)
  (venv-initialize-interactive-shells)
  (venv-initialize-eshell)
#+END_SRC
** Prodigy (background services)
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package prodigy
    :init
    (prodigy-define-service
     :name "nikola"
     :command "nikola"
     :args '("serve")
     :cwd "/home/grahnen/Org/blog/grahnen.se/"
     :tags '(blog nikola)
     :stop-signal 'sigint
     :init (lambda () (venv-workon "blog"))))
#+END_SRC
* Magit
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package magit
    :general
    ("C-x g" 'magit-status))
#+END_SRC
* Dired
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package dired-subtree
    :general
    (dired-mode-map
     "<tab>" 'dired-subtree-toggle))

  (use-package dired-filter
    :general
    (dired-mode-map
     "§" 'dired-filter-mode
     "'" dired-filter-map
     "å" 'dired-filter-group-mode)
    :config
    (setq dired-filter-group-saved-groups
          '(("Default"
             ("PDF"
              (extension . "pdf"))
             ("LaTeX"
              (extension "tex" "bib"))
             ("Org"
              (extension . "org"))
             ("Archives"
              (extension "zip" "rar" "tar" "gz" "bz2"))))))
#+END_SRC
* Org-Mode
** Options
#+BEGIN_SRC emacs-lisp :tangle yes
  (setq org-directory "~/Org"
        org-default-notes-file (concat org-directory "/notes.org")
        org-export-hide-leading-stars t
        org-startup-folded 'overview
        org-startup-indenteded t
        org-hide-emphasis-markers t
        org-pretty-entities t
        org-ellipsis "  "
        org-fontify-whole-heading-line t
        org-fontify-done-headline t
        org-fontify-quote-and-verse-blocks t
        org-support-shift-select t
        org-confirm-babel-evaluate nil
        org-property-inherit-p t
        org-agenda-files (mapcar (lambda (x) (concat org-directory "/" x))
                                 '("notes.org" "schedule.org")))


#+END_SRC
** Babel Languages
#+BEGIN_SRC emacs-lisp :tangle yes
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((shell . t)
     (emacs-lisp . t)
     (python . t)
     (haskell . t)
     (gnuplot . t)))
#+END_SRC
** Default Programs
   #+BEGIN_SRC emacs-lisp :tangle yes
   (setcdr (assoc "\\.pdf\\'" org-file-apps) "okular %s")
   #+END_SRC
** Export Templates
*** HTMLize
#+BEGIN_SRC emacs-lisp :tangle yes
(use-package htmlize)
#+END_SRC
*** Reveal.js
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package ox-reveal
    :config
    (setq org-reveal-root "https://cdn.jsdelivr.net/reveal.js/3.0.0/"))
#+END_SRC
** Blog (Nikola)
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package blog-admin
    :init
    (setq blog-admin-backend-type 'nikola
          blog-admin-backend-path "~/Org/blog/grahnen.se/"
          blog-admin-backend-new-posts-in-drafts t
          blog-admin-backend-nikola-executable "~/.virtualenvs/blog/bin/nikola"
          blog-admin-backend-new-post-with-same-name-dir t))

#+END_SRC
** Calendar
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package calfw)
  (use-package calfw-org)
#+END_SRC

** CalDAV
#+BEGIN_SRC emacs-lisp :tangle yes
    (use-package org-caldav
      :config
      (setq org-caldav-calendars
            '((:calendar-id "personal"
                            :files ("~/Org/schedule.org")
                            :inbox "~/Org/caldav-inbox.org")
              (:calendar-id "skola"
                            :files ("~/Org/skola.org")
                            :inbox "~/Org/caldav-inbox.org"))
            org-caldav-url "https://home.grahnen.se/remote.php/dav/calendars/grahnen"))
#+END_SRC
** GNUplot
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package gnuplot
    :straight gnuplot)
#+END_SRC
* Language Configs
** C/C++
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package irony
    :hook
    (c++-mode . irony-mode)
    (c-mode . irony-mode)
    (objc-mode . irony-mode)
    (irony-mode . irony-cdb-autosetup-compile-options))
  (use-package company-irony
    :config
    (eval-after-load 'company
      '(add-to-list 'company-backends 'company-irony)))


  (use-package company-irony-c-headers
    :config
    (eval-after-load 'company
      '(add-to-list
        'company-backends 'company-irony-c-headers)))
#+END_SRC
** Haskell
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package haskell-mode)
  (use-package ghc
    :init (ghc-init))
  (use-package company-ghc
    :config
    (add-to-list 'company-backends 'company-ghc))
#+END_SRC
** C#
#+BEGIN_SRC emacs-lisp
  (use-package omnisharp
    :hook
    ('csharp-mode-hook 'omnisharp-mode)
    :config
    (setq omnisharp-expected-server-version "1.32.11")
    (add-to-list 'company-backends 'company-omnisharp))
#+END_SRC
** $\LaTeX$
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package tex-site
    :straight auctex)
#+END_SRC
* Snippets
#+BEGIN_SRC emacs-lisp
  (use-package yasnippet
    :config (yas-global-mode 1)
    :general
    (yas-minor-mode-map "TAB" nil))
  (use-package yasnippet-snippets)
#+END_SRC
* Which-key
#+BEGIN_SRC emacs-lisp :tangle yes
(use-package which-key
  :config (which-key-mode))
#+END_SRC
* Windowing
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package eyebrowse
    :config
    (eyebrowse-mode t)
    :general
    ("C-M-q" 'eyebrowse-prev-window-config)
    ("C-M-w" 'eyebrowse-next-window-config))
#+END_SRC
* Navigation
** Projectile
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package projectile
    :config
    (projectile-mode +1)
    :general
    ("C-c p" 'projectile-command-map))
#+END_SRC

** Visual-Regexp (search and replace)
#+BEGIN_SRC emacs-lisp :tangle yes
(use-package visual-regexp-steroids)
#+END_SRC
** Jump Mode
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package avy
    :general
    ("C-." 'avy-goto-char-2))
#+END_SRC
* Custom Leader
For easy access to useful functions
#+BEGIN_SRC emacs-lisp :tangle yes
  (general-define-key
   :prefix "C-å"
   "s" 'cfw:open-org-calendar
   "c" 'org-capture
   "e" 'eshell
   "C-s" 'org-caldav-sync
   "p" 'prodigy
   "b" 'blog-admin-start
   "i" (find-lambda (concat user-emacs-directory "init.el")))
#+END_SRC
* Visual
** Tweaks
#+BEGIN_SRC emacs-lisp :tangle yes
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)
(blink-cursor-mode -1)
(global-hl-line-mode t)
(show-paren-mode t)
#+END_SRC
** Theme
#+BEGIN_SRC emacs-lisp
  (use-package dracula-theme
    :config (load-theme 'dracula t))
#+END_SRC
** org-mode
*** Bullets
#+BEGIN_SRC emacs-lisp
  (use-package org-bullets
    :hook (org-mode . org-bullets-mode))
#+END_SRC
** Modeline
#+BEGIN_SRC emacs-lisp :tangle yes
  (use-package doom-modeline
    :hook (after-init . doom-modeline-mode)
    :config
    (setq doom-modeline-icon t
          doom-modeline-major-mode-icon t))
#+END_SRC
