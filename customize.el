(message "Loaded customize.el")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-idle-delay 0.1)
 '(company-minimum-prefix-length 1)
 '(company-require-match nil)
 '(company-show-numbers nil)
 '(company-tooltip-align-annotations t)
 '(dired-filter-saved-filters (quote (("Org-mode" (extension . "org")))))
 '(global-company-mode t)
 '(package-selected-packages
   (quote
    (quelpa delight quelpa-use-package general counsel company-box auto-package-update async))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#282a36" :foreground "#f8f8f2" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 240 :width normal :foundry "SRC" :family "Hack")))))
